@extends('admin.layout.layout')
@section('content')

    <div class="content-body">
       <div class="container-fluid">
         <div class="row">
             {{--     Medicine Company Add--}}
             <div class="col-xl-6 col-lg-6 ">
                   <div class="card">
                             <div class="card-header">
                                <h4 class="card-title">Medicine Company Add</h4>
                              </div>
                         <div class="card-body">
                             <div class="basic-form">
                              <form>
                         <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Company Name</label>
                                <input type="text" class="form-control" placeholder="Medincine Company Name">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Phone</label>
                                <input type="email" class="form-control" placeholder="Phone No">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Address</label>
                                <input type="password" class="form-control" placeholder="address">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Order Date</label>
                                <input type="date" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label>Order Taker</label>
                                <select id="inputState" class="form-control default-select">
                                    <option selected>Choose...</option>
                                    <option>Tahir</option>
                                    <option>Tayyab</option>
                                    <option>Tanveer</option>
                                </select>
                            </div>
                            <div class="form-group col-md-2">
                                <label>Status</label>
                                <input type="text" class="form-control">
                            </div>
                         </div>

                        <button type="submit" class="btn btn-primary">Save</button>
                         </form>
                             </div>
                         </div>
                   </div>
             </div>

{{--             order Taker--}}
             <div class="col-xl-6 col-lg-6">
                <div class="card">
                       <div class="card-header">
                           <h4 class="card-title">order Taker</h4>
                       </div>
                     <div class="card-body">
                         <div class="basic-form">
                            <form>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Name</label>
                                        <input type="text" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>ompany Id</label>
                                        <input type="email" class="form-control" placeholder="Company id here ">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Status</label>
                                        <select id="status-id" class="form-control default-select">
                                            <option selected>Choose...</option>
                                            <option>Tahir</option>
                                            <option>Tayyab</option>
                                            <option>Tanveer</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Order Date</label>
                                        <input type="date" class="form-control">
                                    </div>
                                </div>
                         </div>
                                <button type="submit" class="btn btn-primary">Save</button>
                            </form>
                        </div>
                     </div>
             </div>

{{--    Company Details--}}

              <div class="col-xl-6 col-lg-6 ">
                <div class="card">
                    <div class="card-header">
                         <h4 class="card-title">Company Details</h4>
                    </div>
                  <div class="card-body">
                    <div class="basic-form">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control" placeholder="Medincine Company Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Phone</label>
                                    <input type="email" class="form-control" placeholder="Phone No">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Address</label>
                                    <input type="password" class="form-control" placeholder="address">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Order Date</label>
                                    <input type="date" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Order Taker</label>
                                    <select id="inputState" class="form-control default-select">
                                        <option selected>Choose...</option>
                                        <option>Tahir</option>
                                        <option>Tayyab</option>
                                        <option>Tanveer</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Status</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>
                    </div>
                  </div>
                </div>
              </div>

{{--    Company order--}}
                <div class="col-xl-6 col-lg-6 ">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Company order</h4>
                        </div>
                        <div class="card-body">
                            <div class="basic-form">
                             <form>
                                 <div class="form-row">
                                     <div class="form-group col-md-6">
                                         <label>Select Company Name:</label>
                                         <select class="form-control default-select" id="sel1">
                                       <option>1</option>
                                       <option>2</option>
                                       <option>3</option>
                                       <option>4</option>
                                </select>
                                </div>
                                    <div class="form-group col-md-6">
                                        <label>Order Take</label>
                                        <input type="date" class="form-control" placeholder="Order Taker Name">
                                </div>

                                    <div class="form-group col-md-6">
                                        <label>Medicine</label>
                                        <input type="text" class="form-control" placeholder="Medicine Name">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Sala Rate</label>
                                        <input type="text" class="form-control" placeholder="Sale rate">
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Purchase Rate</label>
                                        <input type="text" class="form-control" placeholder="purchase rate">
                                    </div>

                                   <div class="form-group col-md-6">
                                      <label>Discount Rate</label>
                                      <input type="text" class="form-control" placeholder="discount rate">
                                   </div>

                                   <div class="form-group col-md-6">
                                        <label class="mr-sm-2">Order Made by</label>
                                          <select class="mr-sm-2 default-select" id="inlineFormCustomSelect">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                          </select>
                                    </div>
                                </div>
                                 <button type="submit" class="btn btn-primary">Save</button>
                             </form>
                            </div>
                        </div>
                     </div>
                </div>
{{--     Order Receive--}}
             <div class="col-xl-6 col-lg-6 ">
                 <div class="card">
                     <div class="card-header">
                         <h4 class="card-title">Order Receive</h4>
                     </div>
                     <div class="card-body">
                         <div class="basic-form">
                             <form>
                                 <div class="form-row">
                                     <div class="form-group col-md-6">
                                         <label>Order Receiver Name:</label>
                                         <input type="text" class="form-control" placeholder="Order Reciever Name">
                                     </div>
                                     <div class="form-group col-md-6">
                                         <label>Order Receiver Date:</label>
                                         <input type="date" class="form-control" placeholder="Order Reciever date">
                                     </div>
                                     <div class="form-group col-md-6">
                                         <label>Order Delivery Boy</label>
                                         <input type="text" class="form-control" placeholder="Order delivered  Name">
                                     </div>

                                     <div class="form-group col-md-6">
                                         <label>Total Bill</label>
                                         <input type="text" class="form-control" placeholder="Total bill">
                                     </div>

                                     <div class="form-group col-md-6">
                                         <label>Medicines</label>
                                         <input type="text" class="form-control" placeholder="Receive medicine">
                                     </div>
                                 </div>
                                 <button type="submit" class="btn btn-primary">Save</button>
                             </form>
                         </div>
                     </div>
                 </div>
             </div>



            </div>
       </div>
    </div>
@endsection
