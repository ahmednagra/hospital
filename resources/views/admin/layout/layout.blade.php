<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Hospital Admin Dashboard</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('backend-assets/images/favicon.png')}}">
    <link href="{{asset('backend-assets/vendor/jqvmap/css/jqvmap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('backend-assets/vendor/chartist/css/chartist.min.css')}}">
    <link href="{{asset('backend-assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css')}}" rel="stylesheet">
    <link href="{{asset('backend-assets/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('backend-assets/css/style.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
</head>
<body>


@extends('admin.layout.header')
@yield('content')
@extends('admin.layout.footer')

</body>
<!--**********************************
    Scripts
***********************************-->
<!-- Required vendors -->
<script src="{{asset('backend-assets/vendor/global/global.min.js')}}"></script>
<script src="{{asset('backend-assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backend-assets/vendor/chart.js/Chart.bundle.min.js')}}"></script>
<script src="{{asset('backend-assets/js/custom.min.js')}}"></script>
<script src="{{asset('backend-assets/js/deznav-init.js')}}"></script>
<script src="{{asset('backend-assets/vendor/bootstrap-datetimepicker/js/moment.js')}}"></script>
<script src="{{asset('backend-assets/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- Chart piety plugin files -->
<script src="{{asset('backend-assets/vendor/peity/jquery.peity.min.js')}}"></script>
<!-- Apex Chart -->
<script src="{{asset('backend-assets/vendor/apexchart/apexchart.js')}}"></script>

<!-- Dashboard 1 -->
<script src="{{asset('backend-assets/js/dashboard/dashboard-1.js')}}"></script>
<script>
    $(function () {
        $('#datetimepicker1').datetimepicker({
            inline: true,
        });
    });
</script>
<!-- Datatable -->
<script src="{{asset('backend-assets/vendor/datatables/js/jquery.dataTables.min.js')}}"></script>
<script>
    (function($) {
        var table = $('#example5').DataTable({
            searching: false,
            paging:true,
            select: false,
            //info: false,
            lengthChange:false
        });
        $('#example tbody').on('click', 'tr', function () {
            var data = table.row( this ).data();
        });
    })(jQuery);
</script>
</html>
