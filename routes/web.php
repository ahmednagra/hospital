<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BaseController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
// self base Controllers
Route::get('/home', [BaseController::class,'index'])->name('home');
Route::get('/about', [BaseController::class,'about'])->name('about');
Route::get('/services', [BaseController::class,'service'])->name('service');
//Route::get('/doctors', [BaseController::class,'doctor'])->name('doctor');
Route::get('/blog', [BaseController::class,'blog'])->name('blog');
Route::get('/contact-us', [BaseController::class,'contact'])->name('contact');

// route made after install  jetstream with livewire
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// Admin Controller
Route::get('/admin',[AdminController::class, 'index'])->name('admindashboard');
Route::group(['namespace' => 'Admin','prefix' => 'admin'], function (){
    Route::get('/doctors', [AdminController::class,'doctor'])->name('doctor');
    Route::get('/lab', [AdminController::class,'lab'])->name('lab');
    Route::get('/patient', [AdminController::class,'patient'])->name('patient');
    Route::get('/review', [AdminController::class,'review'])->name('review');
    Route::get('/profile', [AdminController::class,'profile'])->name('profile');
    Route::get('/mail', [AdminController::class,'mail'])->name('mail');
    Route::get('/laboratory', [AdminController::class,'laboratory'])->name('laboratory');

    // Pharmacy Routes
    Route::get('/pharmacy', [AdminController::class,'home'])->name('pharmacyhome');
    Route::get('/pharmacy/form', [AdminController::class,'form'])->name('pharmacyform');
    Route::get('/pharmacy/medicinecomp', [AdminController::class,'medicinecomp'])->name('medicinecompany');
    Route::get('/pharmacy/po', [AdminController::class,'po'])->name('purchaseorder');
    Route::get('/pharmacy/poreceive', [AdminController::class,'poreceive'])->name('poreceive');
});


//Patient Controller
//Route::resource('patient', \App\Http\Controllers\PatientController::class);
