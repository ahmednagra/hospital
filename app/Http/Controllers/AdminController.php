<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.layout.home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function patient(){

//        echo 'patient section';
        return view( 'admin.pages.patient');
    }
    public function doctor(){
        //
        return view('admin.pages.doctor');
    }
    public function review(){
        //
        return view('admin.pages.review');
    }
    public function mail(){
        //
        return view('admin.pages.mail');
    }
    public function profile(){
        //
        return view('admin.pages.profile');
    }

public function laboratory(){
    //
    return view('admin.pages.laboratory');
    }

    // Pharmacy section
    public function home(){
        //
        return view('admin.Pharmacy.home');
    }
    public function medicinecomp(){
        //
        return view('admin.Pharmacy.medicinecompany');
    }

public function po(){
    //
    return view('admin.Pharmacy.purchaseorder');
}
    public function form(){
        //
        return view('admin.pages.pharmacy');
    }
    public function poreceive(){
        //
        return view('admin.Pharmacy.poreceiving');
    }
}
