<?php

namespace App\Http\Controllers;

use App\Models\physician;
use Illuminate\Http\Request;

class PhysicianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\physician  $physician
     * @return \Illuminate\Http\Response
     */
    public function show(physician $physician)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\physician  $physician
     * @return \Illuminate\Http\Response
     */
    public function edit(physician $physician)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\physician  $physician
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, physician $physician)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\physician  $physician
     * @return \Illuminate\Http\Response
     */
    public function destroy(physician $physician)
    {
        //
    }
}
